package com.interactor;

import com.repository.SignInRepository;

/**
 * Created by Andres Rubiano Del Chiaro on 16/04/16.
 */
public class SignInInteractor {

    private final SignInRepository signInRepository;

    public SignInInteractor(SignInRepository signInRepository) {
        this.signInRepository = signInRepository;
    }

    public void onSignIn(final ServerCallback serverCallback, int idUser)
    {
        signInRepository.onSignIn(serverCallback, idUser);
    }
}
