package com.interactor;

import com.model.Album;

import java.util.List;

/**
 * Created by Andres Rubiano Del Chiaro on 27/09/2016.
 */

public interface AlbumCallback {

    void onAlbumSuccess(List<Album> album);
    void onAlbumError(String message);

}
