package com.interactor;

import com.repository.AlbumRepository;

/**
 * Created by Andres Rubiano Del Chiaro on 16/04/16.
 */
public class AlbumInteractor {

    private final AlbumRepository albumRepository;

    public AlbumInteractor(AlbumRepository albumRepository) {
        this.albumRepository = albumRepository;
    }

    public void onLoadAlbums(final AlbumCallback signInCallback, int idUser)
    {
        albumRepository.getAlbums(signInCallback, idUser);
    }
}
