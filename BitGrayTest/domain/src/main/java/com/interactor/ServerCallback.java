package com.interactor;

import com.model.User;

/**
 * Created by Andres Rubiano Del Chiaro on 27/09/16.
 */
public interface ServerCallback {

    void onThemeSuccess(User user);
    void onThemeError(String message);

}
