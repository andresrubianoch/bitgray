package com.repository;

import com.interactor.AlbumCallback;

/**
 * Created by Andres Rubiano Del Chiaro on 27/09/2016.
 */

public interface AlbumRepository {

    void getAlbums(final AlbumCallback albumCallback, int idUser);

    void getPhotos(final AlbumCallback albumCallback, int idUser);

}
