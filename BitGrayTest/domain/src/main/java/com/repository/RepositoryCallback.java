package com.repository;

/**
 * Created by Andres Rubiano Del Chiaro on 27/09/2016.
 */
public interface RepositoryCallback {

    void onError(Object object);
    void onSuccess(Object object);
}
