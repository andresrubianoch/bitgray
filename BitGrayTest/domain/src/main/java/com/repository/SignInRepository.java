package com.repository;

import com.interactor.ServerCallback;

/**
 * Created by Andres Rubiano Del Chiaro on 16/04/16.
 */
public interface SignInRepository {

    void onSignIn(final ServerCallback serverCallback, int idUser);

}
