package com.mituapps.bitgraytest.utils.media;

import android.widget.ImageView;

/**
 * Created by Andres Rubiano Del Chiaro on 16/04/16.
 */
public class PicassoILoader implements ImageLoader {

    @Override
    public void load(String url, ImageView imageView) {

    }

    @Override
    public void loadLocal(String path, ImageView imageView) {

    }
}
