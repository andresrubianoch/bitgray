package com.mituapps.bitgraytest.ui.activities;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import com.mituapps.bitgraytest.R;
import com.mituapps.bitgraytest.models.ObjAlbumView;
import com.mituapps.bitgraytest.presenter.AlbumPresenterImpl;
import com.mituapps.bitgraytest.ui.fragments.ListFragment;
import com.mituapps.bitgraytest.view.AlbumView;

import java.util.ArrayList;

import butterknife.Bind;

public class AlbumActivity extends BaseActivity implements AlbumView{

    @Bind(R.id.containerAlbum)
    FrameLayout containerAlbum;
    @Bind(R.id.albumProgressBar)
    ProgressBar albumProgressBar;

    private AlbumPresenterImpl mAlbumPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album);

        injectView();
        setUpPresenter();
        loadAlbums();
    }

    private void setUpPresenter() {
        if (null == mAlbumPresenter){
            mAlbumPresenter = new AlbumPresenterImpl();
            mAlbumPresenter.addView(AlbumActivity.this);
        }
    }

    @Override
    public void loadAlbums() {
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                AlbumActivity.this.mAlbumPresenter.onLoadAlbums(getSessionUser());
            }
        });
    }

    @Override
    public void onSuccessAlbumDownload(ArrayList<ObjAlbumView> albums) {
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(ListFragment.ARRAY, albums);
        addFragment(R.id.containerAlbum, ListFragment.newInstance(bundle));
    }

    @Override
    public void hideProgress() {
        hideView(albumProgressBar);
    }

    @Override
    public void showProgress() {
        showView(albumProgressBar);
    }

    @Override
    public void showContainer() {
        showView(containerAlbum);
    }

    @Override
    public void hideContainer() {
        hideView(containerAlbum);
    }

    @Override
    public void enableInputs() {

    }

    @Override
    public void disableInputs() {

    }

    @Override
    public void onErrorMessage(String message) {
        showSnackbarMessage(containerAlbum, message);
    }

    @Override
    public void noInternetConnection() {
        showSnackbarMessage(containerAlbum,
                getString(R.string.no_internet),
                getString(R.string.no_internet_retry),
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        AlbumActivity.this.loadAlbums();
                    }
                });
    }

    @Override
    public Context getContext() {
        return AlbumActivity.this;
    }

    @Override
    protected void onDestroy() {
        AlbumActivity.this.mAlbumPresenter.removeView(this);
        super.onDestroy();
    }
}
