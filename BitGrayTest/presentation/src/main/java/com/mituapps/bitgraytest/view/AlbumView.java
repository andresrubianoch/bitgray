package com.mituapps.bitgraytest.view;

import com.mituapps.bitgraytest.models.ObjAlbumView;

import java.util.ArrayList;

/**
 * Created by Andres Rubiano Del Chiaro on 27/09/2016.
 */

public interface AlbumView extends BaseUI{

    void loadAlbums();
    void onSuccessAlbumDownload(ArrayList<ObjAlbumView> albums);
}
