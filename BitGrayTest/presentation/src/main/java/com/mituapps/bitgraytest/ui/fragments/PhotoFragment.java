package com.mituapps.bitgraytest.ui.fragments;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mituapps.bitgraytest.R;
import com.mituapps.bitgraytest.adapter.AlbumAdapter;
import com.mituapps.bitgraytest.adapter.PhotoAdapter;
import com.mituapps.bitgraytest.models.ObjAlbumView;
import com.mituapps.bitgraytest.models.ObjPhotoView;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

public class PhotoFragment extends BaseFragment {

    @Bind(R.id.toolbar_photo)
    Toolbar toolbarFragment;
    @Bind(R.id.photo_text_title)
    TextView photoTextTitle;
    private ArrayList<ObjPhotoView> listPhoto;
    private PhotoAdapter mAdapter;

    @Bind(R.id.recyclerPhoto)
    RecyclerView recyclerPhoto;

    public PhotoFragment() {
    }

    public static PhotoFragment newInstance(Bundle arguments) {
        PhotoFragment f = new PhotoFragment();
        if (arguments != null) {
            f.setArguments(arguments);
        }
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_photo, container, false);
        ButterKnife.bind(this, view);
        initList();
        setRetainInstance(true);

        initToolbar();
        initRecycler();
        initAdapter();
        return view;
    }

    private void initToolbar() {
        setUpToolbar(toolbarFragment);
    }

    private void initRecycler() {
        setUpRecycler(photoTextTitle, recyclerPhoto);
    }

    private void initAdapter() {
        mAdapter = new PhotoAdapter(getActivity());
        recyclerPhoto.setAdapter(mAdapter);
        ObjPhotoView al = new ObjPhotoView("reprehenderit est deserunt velit ipsam", "http://placehold.it/600/92c952");
        final ArrayList<ObjPhotoView> list = new ArrayList<>();
        list.add(al);
        al = new ObjPhotoView("reprehenderit est deserunt velit ipsam", "http://placehold.it/600/771796");
        list.add(al);
        al = new ObjPhotoView("officia porro iure quia iusto qui ipsa ut modi", "http://placehold.it/600/24f355");
        list.add(al);
        al = new ObjPhotoView("culpa odio esse rerum omnis laboriosam voluptate repudiandae", "http://placehold.it/600/d32776");
        list.add(al);
        al = new ObjPhotoView("natus nisi omnis corporis facere molestiae rerum in", "http://placehold.it/600/f66b97");
        list.add(al);
        al = new ObjPhotoView("accusamus ea aliquid et amet sequi nemo", "http://placehold.it/600/56a8c2");
        list.add(al);
        al = new ObjPhotoView("officia delectus consequatur vero aut veniam explicabo molestias", "http://placehold.it/600/b0f7cc");
        list.add(al);

        setUpToolbarAnimation(toolbarFragment, new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                mAdapter.addAll(list);
            }
        });
    }

    private void initList() {
        listPhoto = new ArrayList<>();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
