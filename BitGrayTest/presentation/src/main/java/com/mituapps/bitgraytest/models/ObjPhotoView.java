package com.mituapps.bitgraytest.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Andres Rubiano Del Chiaro on 28/09/2016.
 */

public class ObjPhotoView implements Parcelable {

    private String title;
    private String url;

    public ObjPhotoView() {
    }

    public ObjPhotoView(String title, String url) {
        this.title = title;
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeString(this.url);
    }

    protected ObjPhotoView(Parcel in) {
        this.title = in.readString();
        this.url = in.readString();
    }

    public static final Parcelable.Creator<ObjPhotoView> CREATOR = new Parcelable.Creator<ObjPhotoView>() {
        @Override
        public ObjPhotoView createFromParcel(Parcel source) {
            return new ObjPhotoView(source);
        }

        @Override
        public ObjPhotoView[] newArray(int size) {
            return new ObjPhotoView[size];
        }
    };
}
