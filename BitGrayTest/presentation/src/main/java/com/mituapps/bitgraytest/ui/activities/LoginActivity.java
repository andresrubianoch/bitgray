package com.mituapps.bitgraytest.ui.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.mituapps.bitgraytest.R;
import com.mituapps.bitgraytest.presenter.ServerPresenter;
import com.mituapps.bitgraytest.view.MainView;
import com.model.User;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends BaseActivity implements MainView {

    private static final int TIME_TO_WAIT = 1500;
    private static final String CREDENTIALS = "usuario";

    @Bind(R.id.inputEmail)
    EditText inputEmail;
    @Bind(R.id.inputPassword)
    EditText inputPassword;
    @Bind(R.id.login_progress)
    ProgressBar loginProgress;
    @Bind(R.id.wrapperEmail)
    TextInputLayout wrapperEmail;
    @Bind(R.id.wrapperPassword)
    TextInputLayout wrapperPassword;
    @Bind(R.id.email_sign_in_button)
    Button buttonSignIn;
    @Bind(R.id.containerSignIn)
    LinearLayout containerSignIn;

    private ServerPresenter mSignInPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        injectView();
        if (getSessionUser() == null){
            init();
        } else {
            goToMenuActivity();
        }


        inputPassword.setText("usuario");
        inputEmail.setText("usuario");
    }

    private void init() {
        if (mSignInPresenter == null)
            mSignInPresenter = new ServerPresenter();
        mSignInPresenter.addView(this);
    }

    @OnClick(R.id.email_sign_in_button)
    public void onClick() {
        onClickSignIn();
    }

    private void onClickSignIn() {
        if (null != mSignInPresenter) {
            mSignInPresenter.onSignIn(getStatusConnection());
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
    }

    @Override
    public void showLoading() {
        loginProgress.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        loginProgress.setVisibility(View.GONE);
    }

    @Override
    public void enableInputs(boolean status) {
        inputEmail.setEnabled(status);
        inputPassword.setEnabled(status);
        buttonSignIn.setEnabled(status);
    }

    @Override
    public void showErrorMessage(String message) {
        showSnackbarMessage(containerSignIn, message);
    }

    @Override
    public void goToMenuActivity() {
        mNavigator.toMenuActivity(LoginActivity.this);
        LoginActivity.this.finish();
    }

    @Override
    public void noInternetConnection() {
        showSnackbarMessage(containerSignIn,
                getString(R.string.no_internet),
                getString(R.string.no_internet_retry),
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        onClickSignIn();
                    }
                });
    }

    @Override
    public Context getContext() {
        return LoginActivity.this;
    }

    @Override
    public boolean validateEmail() {
        String email = inputEmail.getText().toString().trim();

        if (email.isEmpty()) {
            wrapperEmail.setError(getString(R.string.err_msg_email));
            requestFocus(inputEmail);
            return false;
        } else {
            wrapperEmail.setErrorEnabled(false);
        }

        return true;
    }

    @Override
    public boolean validatePassword() {
        if (inputPassword.getText().toString().trim().isEmpty()) {
            wrapperPassword.setError(getString(R.string.err_msg_password));
            requestFocus(inputPassword);
            return false;
        } else {
            wrapperPassword.setErrorEnabled(false);
        }

        return true;
    }

    @Override
    public boolean validateCredentials() {
        String email = inputEmail.getText().toString().trim();
        if (email.equals(CREDENTIALS)) {
            String password = inputPassword.getText().toString().trim();
            if (password.equals(CREDENTIALS)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void addUserSession(User user) {
        startSession(user);
    }

}

