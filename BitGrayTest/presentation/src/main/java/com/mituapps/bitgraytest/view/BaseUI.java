package com.mituapps.bitgraytest.view;

/**
 * Created by Andres Rubiano Del Chiaro on 27/09/2016.
 */

public interface BaseUI extends BaseView {

    void hideProgress();
    void showProgress();

    void showContainer();
    void hideContainer();

    void enableInputs();
    void disableInputs();

    void onErrorMessage(String message);

    void noInternetConnection();

}
