package com.mituapps.bitgraytest.utils.app;

import android.app.Application;

import com.model.User;

/**
 * Created by Andres Rubiano Del Chiaro on 23/09/2016.
 */

public class AndroidApplication extends Application {

    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
