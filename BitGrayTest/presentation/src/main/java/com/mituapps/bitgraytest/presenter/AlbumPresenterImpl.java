package com.mituapps.bitgraytest.presenter;

import android.support.annotation.NonNull;
import android.util.Log;

import com.interactor.AlbumCallback;
import com.interactor.AlbumInteractor;
import com.mituapps.bitgraytest.models.ObjAlbumView;
import com.mituapps.bitgraytest.view.AlbumView;
import com.mituapps.data.datasource.DataStoreFactory;
import com.mituapps.data.mapper.DataMapper;
import com.mituapps.data.repository.AlbumRepositoryImpl;
import com.model.Album;
import com.model.User;
import com.repository.AlbumRepository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andres Rubiano Del Chiaro on 27/09/2016.
 */

public class AlbumPresenterImpl implements Presenter<AlbumView>, AlbumCallback{

    private static final int ERR_NO_INTERNET = 1;
    private AlbumView albumView;
    private AlbumInteractor albumInteractor;

    public void onLoadAlbums(@NonNull  User user){
        this.albumView.disableInputs();
        this.albumView.showProgress();
        this.albumInteractor.onLoadAlbums(this, user.getId());
    }

    @Override
    public void onAlbumSuccess(List<Album> albums) {
        for (Album album : albums){
            Log.e("album", album.toString());
        }
        AlbumPresenterImpl.this.albumView.hideProgress();
        AlbumPresenterImpl.this.albumView.enableInputs();
        AlbumPresenterImpl.this.albumView.showContainer();
        AlbumPresenterImpl.this.albumView.onSuccessAlbumDownload(mapperAlbum(albums));
    }

    private ArrayList<ObjAlbumView> mapperAlbum(List<Album> albums) {
        ArrayList<ObjAlbumView> list = new ArrayList<>();
        for(Album album : albums){
            list.add(new ObjAlbumView(album.getUserId(),
                                        album.getId(),
                                        album.getTitle()));
        }
        return list;
    }

    @Override
    public void onAlbumError(String message) {

    }

    @Override
    public void addView(AlbumView view) {
        this.albumView = view;
        AlbumRepository albumRepository = new AlbumRepositoryImpl(
                                                new DataStoreFactory(
                                                                this.albumView.getContext()),
                                                                new DataMapper());
        albumInteractor = new AlbumInteractor(albumRepository);
    }

    @Override
    public void removeView(AlbumView view) {
        this.albumView = null;
    }

}
