package com.mituapps.bitgraytest.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Andres Rubiano Del Chiaro on 28/09/2016.
 */

public class ObjAlbumView implements Parcelable{

    private int userId;
    private int id;
    private String title;

    public ObjAlbumView(int userId, int id, String title) {
        this.userId = userId;
        this.id = id;
        this.title = title;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.userId);
        dest.writeInt(this.id);
        dest.writeString(this.title);
    }

    protected ObjAlbumView(Parcel in) {
        this.userId = in.readInt();
        this.id = in.readInt();
        this.title = in.readString();
    }

    public static final Creator<ObjAlbumView> CREATOR = new Creator<ObjAlbumView>() {
        @Override
        public ObjAlbumView createFromParcel(Parcel source) {
            return new ObjAlbumView(source);
        }

        @Override
        public ObjAlbumView[] newArray(int size) {
            return new ObjAlbumView[size];
        }
    };
}
