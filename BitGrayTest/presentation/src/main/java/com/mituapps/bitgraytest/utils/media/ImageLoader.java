package com.mituapps.bitgraytest.utils.media;

import android.widget.ImageView;

/**
 * Created by Andres Rubiano Del Chiaro on 23/09/2016.
 */

public interface ImageLoader {

    void load(String url, ImageView imageView);
    void loadLocal(String path, ImageView imageView);
}
