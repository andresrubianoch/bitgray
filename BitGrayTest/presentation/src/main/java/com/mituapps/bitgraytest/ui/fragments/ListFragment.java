package com.mituapps.bitgraytest.ui.fragments;


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mituapps.bitgraytest.R;
import com.mituapps.bitgraytest.adapter.AlbumAdapter;
import com.mituapps.bitgraytest.models.ObjAlbumView;
import com.mituapps.bitgraytest.ui.activities.PhotoActivity;
import com.mituapps.bitgraytest.ui.recycler.RecyclerClickListener;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ListFragment extends BaseFragment {

    @Bind(R.id.text_title)
    TextView textTitle;
    @Bind(R.id.toolbar_fragment)
    Toolbar toolbarFragment;
    @Bind(R.id.recycler_adapter)
    RecyclerView recyclerAdapter;

    private AlbumAdapter mAdapter;

    public static final String ARRAY = "arrayList";
    private ArrayList<ObjAlbumView> mArrayInfo;

    public ListFragment() {
    }

    public static ListFragment newInstance(Bundle arguments) {
        ListFragment f = new ListFragment();
        if (arguments != null) {
            f.setArguments(arguments);
        }
        return f;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.fragment_album, container, false);

        ButterKnife.bind(this, v);

        Bundle bundle = getArguments();
        mArrayInfo = new ArrayList<>();
        mArrayInfo.addAll(bundle.<ObjAlbumView>getParcelableArrayList(ARRAY));

        setRetainInstance(true);

        initToolbar();
        initRecycler();
        initAdapter();

        return v;
    }

    private void initToolbar() {
        setUpToolbar(toolbarFragment);
    }

    private void initRecycler() {
        setUpRecycler(textTitle,
                recyclerAdapter,
                new RecyclerClickListener() {
                    @Override
                    public void onClick(View view, int position) {
                        Intent intent = new Intent(getActivity(), PhotoActivity.class);
                        getActivity().startActivity(intent);
                    }

                    @Override
                    public void onLongClick(View view, int position) {

                    }
                });
    }

    private void initAdapter() {
        mAdapter = new AlbumAdapter(getActivity());
        recyclerAdapter.setAdapter(mAdapter);
        setUpToolbarAnimation(toolbarFragment, new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);

                // Fire item animator
                mAdapter.addAll(mArrayInfo);

                // Animate fab
//                ViewCompat.animate(mFab).setStartDelay(600)
//                        .setDuration(400).scaleY(1).scaleX(1).start();

            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
