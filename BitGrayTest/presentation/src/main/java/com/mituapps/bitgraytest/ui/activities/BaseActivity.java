package com.mituapps.bitgraytest.ui.activities;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.mituapps.bitgraytest.R;
import com.mituapps.bitgraytest.utils.app.AndroidApplication;
import com.mituapps.bitgraytest.utils.navigation.Navigator;
import com.model.User;

import butterknife.ButterKnife;

/**
 * Created by Andres Rubiano Del Chiaro on 23/09/2016.
 */

public abstract class BaseActivity extends AppCompatActivity {

    Navigator mNavigator;

    public boolean bIsLargeScreen = false;
    private final static boolean IS_LARGE_SCREEN = true;
    private final static boolean IS_NOT_LARGE_SCREEN = false;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initNavigator();
    }

    protected void initNavigator() {
        if (mNavigator == null)
            mNavigator = new Navigator();
    }

    private void changeOrientation(@NonNull int type){
        setRequestedOrientation(type);
    }

    protected void setLargeScreen(@NonNull boolean flag){
        bIsLargeScreen = flag;
    }

    protected void hideView(@NonNull View v){v.setVisibility(View.GONE);}

    protected void showView(@NonNull View v){v.setVisibility(View.VISIBLE);}

    protected void addFragment(@NonNull int layout, @NonNull Fragment fragment){
        addFragment(layout, fragment, null);
    }

    protected void addFragment(@NonNull int layout,
                               @NonNull Fragment fragment,
                               @Nullable Bundle bundle){
        if (null != bundle){
            fragment.setArguments(bundle);
        }
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(layout, fragment);
        transaction.commit();
    }

    protected void showSnackbarMessage(@NonNull ViewGroup activity, @NonNull String message){
        Snackbar
                .make(activity, message, Snackbar.LENGTH_LONG)
                .show();
    }

    protected void showSnackbarMessage(@NonNull ViewGroup activity,
                                       @NonNull String message,
                                       @NonNull String infoClick,
                                       @NonNull View.OnClickListener onClickListener){
        Snackbar
                .make(activity, message, Snackbar.LENGTH_INDEFINITE)
                .setAction(infoClick, onClickListener)
                .show();
    }

    protected boolean getStatusConnection(){
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    protected void injectView(){
        ButterKnife.bind(this);
    }

    protected boolean isValidEmail(String email) {
        return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    protected void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    protected boolean isSignIn() {
        final AndroidApplication globalVariable = (AndroidApplication) BaseActivity.this.getApplicationContext();
        User user = globalVariable.getUser();
        if (null != user) {
            return true;
        }
        return false;
    }

    protected void startSession(User user) {
        final AndroidApplication globalVariable = (AndroidApplication) BaseActivity.this.getApplicationContext();
        globalVariable.setUser(user);
    }

    protected void closeSession() {
        final AndroidApplication globalVariable = (AndroidApplication) BaseActivity.this.getApplicationContext();
        globalVariable.setUser(null);
    }

    protected String getSessionName() {
        return getSessionUser().getUsername();
    }

    protected User getSessionUser() {
        final AndroidApplication globalVariable = (AndroidApplication) BaseActivity.this.getApplicationContext();

        return globalVariable.getUser();
    }

    protected void initActionBar(@NonNull Toolbar toolbar) {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
}
