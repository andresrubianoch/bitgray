package com.mituapps.bitgraytest.utils.navigation;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;

import com.mituapps.bitgraytest.ui.activities.AlbumActivity;
import com.mituapps.bitgraytest.ui.activities.MenuActivity;
import com.mituapps.bitgraytest.ui.activities.PostActivity;
import com.mituapps.bitgraytest.ui.activities.ProfileActivity;

/**
 * Created by Andres Rubiano Del Chiaro on 23/09/2016.
 */

public class Navigator {

    public Navigator() {
    }

    private Intent getIntent(Context context, @NonNull Class goClass){
        return new Intent(context, goClass);
    }

    public void toMenuActivity(@NonNull Context context){
        if (null != context){
            context.startActivity(getIntent(context, MenuActivity.class));
        } else {
            throw new NullPointerException();
        }
    }

    public void toProfileActivity(@NonNull Context context){
        if (null != context){
            context.startActivity(getIntent(context, ProfileActivity.class));
        } else {
            throw new NullPointerException();
        }
    }

    public void toAlbumActivity(@NonNull Context context){
        if (null != context){
            context.startActivity(getIntent(context, AlbumActivity.class));
        } else {
            throw new NullPointerException();
        }
    }

    public void toPostActivity(@NonNull Context context){
        if (null != context){
            context.startActivity(getIntent(context, PostActivity.class));
        } else {
            throw new NullPointerException();
        }
    }

}
