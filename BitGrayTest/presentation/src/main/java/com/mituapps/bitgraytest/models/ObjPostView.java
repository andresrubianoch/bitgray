package com.mituapps.bitgraytest.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Andres Rubiano Del Chiaro on 28/09/2016.
 */

public class ObjPostView implements Parcelable {

    private int userId;
    private int id;
    private String title;
    private String body;

    public ObjPostView() {
    }

    public ObjPostView(int userId, int id, String title, String body) {
        this.userId = userId;
        this.id = id;
        this.title = title;
        this.body = body;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.userId);
        dest.writeInt(this.id);
        dest.writeString(this.title);
        dest.writeString(this.body);
    }

    protected ObjPostView(Parcel in) {
        this.userId = in.readInt();
        this.id = in.readInt();
        this.title = in.readString();
        this.body = in.readString();
    }

    public static final Parcelable.Creator<ObjPostView> CREATOR = new Parcelable.Creator<ObjPostView>() {
        @Override
        public ObjPostView createFromParcel(Parcel source) {
            return new ObjPostView(source);
        }

        @Override
        public ObjPostView[] newArray(int size) {
            return new ObjPostView[size];
        }
    };
}
