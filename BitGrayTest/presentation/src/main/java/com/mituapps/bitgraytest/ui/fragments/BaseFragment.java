package com.mituapps.bitgraytest.ui.fragments;

import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.TextView;

import com.mituapps.bitgraytest.ui.activities.BaseActivity;
import com.mituapps.bitgraytest.ui.recycler.MarginDecoration;
import com.mituapps.bitgraytest.ui.recycler.RecyclerClickListener;
import com.mituapps.bitgraytest.ui.recycler.RecyclerTouchListener;
import com.mituapps.bitgraytest.utils.animation.ItemAnimatorFactory;

public class BaseFragment extends Fragment {

    protected boolean bIsLargeScreen = false;

    public BaseFragment() {
    }

    protected void setUpRecycler(@NonNull RecyclerView recyclerView,
                                 RecyclerClickListener recyclerClickListener) {
        setUpRecycler(null, recyclerView, recyclerClickListener);
    }

    protected void setUpRecycler(@NonNull TextView view, @NonNull RecyclerView recyclerView) {
        setUpRecycler(view, recyclerView, null);
    }

    protected void setUpRecycler(@NonNull RecyclerView recyclerView) {
        setUpRecycler(null, recyclerView, null);
    }

    protected void setUpRecycler(@NonNull TextView view, @NonNull RecyclerView recyclerView,
                                 RecyclerClickListener recyclerClickListener) {
        if (null != view){
            ViewCompat.animate(view).alpha(1).start();
        }

        if (bIsLargeScreen) {
            RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 3);
        } else {
            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        }
        recyclerView.addItemDecoration(new MarginDecoration(getActivity()));
        recyclerView.setItemAnimator(ItemAnimatorFactory.slidein());
        recyclerView.setHasFixedSize(false);


        if (null != recyclerClickListener){
            recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerView, recyclerClickListener));
        }

    }

    protected void collapseToolbar(@NonNull final Toolbar toolbarFragment, int mContentViewHeight,
                                   AnimatorListenerAdapter animatorListenerAdapter) {
        int toolBarHeight;
        TypedValue tv = new TypedValue();
        getActivity().getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true);
        toolBarHeight = TypedValue.complexToDimensionPixelSize(tv.data, getResources().getDisplayMetrics());
        ValueAnimator valueHeightAnimator = ValueAnimator.ofInt(mContentViewHeight, toolBarHeight);
        valueHeightAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                ViewGroup.LayoutParams lp = toolbarFragment.getLayoutParams();
                lp.height = (Integer) animation.getAnimatedValue();
                toolbarFragment.setLayoutParams(lp);
            }
        });

        valueHeightAnimator.start();
        valueHeightAnimator.addListener(animatorListenerAdapter);
//                new AnimatorListenerAdapter() {
//            @Override
//            public void onAnimationEnd(Animator animation) {
//                super.onAnimationEnd(animation);
//
//                // Fire item animator
//                mAdapter.addAll(mArrayInfo);
//
//                // Animate fab
////                ViewCompat.animate(mFab).setStartDelay(600)
////                        .setDuration(400).scaleY(1).scaleX(1).start();
//
//            }
//        });
    }

    protected void setUpToolbarAnimation(final Toolbar toolbarFragment,
                                         final AnimatorListenerAdapter animatorListenerAdapter) {
        toolbarFragment.getViewTreeObserver().addOnPreDrawListener(
                new ViewTreeObserver.OnPreDrawListener() {
                    @Override
                    public boolean onPreDraw() {
                        toolbarFragment.getViewTreeObserver().removeOnPreDrawListener(this);
                        final int widthSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
                        final int heightSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);

                        toolbarFragment.measure(widthSpec, heightSpec);
                        int mContentViewHeight = toolbarFragment.getHeight();
                        collapseToolbar(toolbarFragment, mContentViewHeight, animatorListenerAdapter);
                        return true;
                    }
                });
    }

    protected void setUpToolbar(@NonNull Toolbar toolbar) {
        BaseActivity activity = (BaseActivity) getActivity();
        activity.setSupportActionBar(toolbar);
        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

}
