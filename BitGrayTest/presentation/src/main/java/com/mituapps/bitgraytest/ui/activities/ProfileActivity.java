package com.mituapps.bitgraytest.ui.activities;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.widget.TextView;

import com.mituapps.bitgraytest.R;
import com.model.User;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ProfileActivity extends BaseActivity {

    @Bind(R.id.labelName)
    TextView labelName;
    @Bind(R.id.labelUsername)
    TextView labelUsername;
    @Bind(R.id.labelEmail)
    TextView labelEmail;
    @Bind(R.id.labelPhone)
    TextView labelPhone;
    @Bind(R.id.labelCompany)
    TextView labelCompany;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);
        injectView();
        setUpInfo();
    }

    private void setUpInfo() {
        User user = getSessionUser();
        setText(labelName, user.getName());
        setText(labelUsername, user.getUsername());
        setText(labelEmail, user.getEmail());
        setText(labelPhone, user.getNumber());
        setText(labelCompany, user.getCompany());
    }

    private void setText(@NonNull TextView view, String text){
        view.setText(text);
    }
}
