package com.mituapps.bitgraytest.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.mituapps.bitgraytest.R;
import com.mituapps.bitgraytest.models.ObjAlbumView;
import com.mituapps.bitgraytest.utils.media.ImageLoaderHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andres Rubiano Del Chiaro on 19/09/2016.
 */

public class AlbumAdapter extends RecyclerView.Adapter<AlbumAdapter.RecyclerViewHolder> {

    private ArrayList<ObjAlbumView> mItems = new ArrayList<>();
    private ImageLoaderHelper imageLoaderHelper;
    private Context context;

    public AlbumAdapter(Context context) {
        this.context = context;
        this.imageLoaderHelper= new ImageLoaderHelper(ImageLoaderHelper.GLIDE);
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_album_row, parent, false);
        return new RecyclerViewHolder(v);
    }

    public void addAll(List<ObjAlbumView> items) {
        int pos = getItemCount();
        mItems.addAll(items);
        notifyItemRangeInserted(pos, mItems.size());
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }


    class RecyclerViewHolder extends RecyclerView.ViewHolder {
        private TextView mTitleTextView;
        private TextView mLabelText;
        private ImageView mImageView;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            mTitleTextView = (TextView) itemView.findViewById(R.id.labelAlbumId);
            mLabelText = (TextView) itemView.findViewById(R.id.labelAlbumTitle);
//            mImageView = (ImageView) itemView.findViewById(R.id.img_sampleimage);
        }

        public void bind(int position) {
//            imageLoaderHelper.getLoader().load(
//                                        mItems.get(position).getUrlImage(),
//                                        mImageView);
//            mImageView.setImageBitmap(BitmapFactory.decodeResource(
//                                            context.getResources(),
//                                            mItems.get(position).getImgId()));
            mTitleTextView.setText(String.valueOf(mItems.get(position).getId()));
            mLabelText.setText(mItems.get(position).getTitle());
        }
    }

}
