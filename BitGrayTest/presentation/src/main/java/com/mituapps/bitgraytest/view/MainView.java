package com.mituapps.bitgraytest.view;


import com.model.User;

/**
 * Created by Andres Rubiano Del Chiaro on 23/09/2016.
 */

public interface MainView extends BaseView {

    void showLoading();
    void hideLoading();
    void enableInputs(boolean status);
    void showErrorMessage(String message);
    void goToMenuActivity();
    void noInternetConnection();

    boolean validateEmail();
    boolean validatePassword();

    boolean validateCredentials();

    void addUserSession(User user);
}
