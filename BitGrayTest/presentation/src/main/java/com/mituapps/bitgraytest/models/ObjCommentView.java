package com.mituapps.bitgraytest.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Andres Rubiano Del Chiaro on 28/09/2016.
 */

public class ObjCommentView implements Parcelable {

    private int postId;
    private int id;
    private String name;
    private String email;
    private String body;

    public ObjCommentView() {
    }

    public ObjCommentView(String name, String email, String body) {
        this.name = name;
        this.email = email;
        this.body = body;
    }

    public ObjCommentView(int postId, int id, String name, String email, String body) {
        this.postId = postId;
        this.id = id;
        this.name = name;
        this.email = email;
        this.body = body;
    }

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.postId);
        dest.writeInt(this.id);
        dest.writeString(this.name);
        dest.writeString(this.email);
        dest.writeString(this.body);
    }

    protected ObjCommentView(Parcel in) {
        this.postId = in.readInt();
        this.id = in.readInt();
        this.name = in.readString();
        this.email = in.readString();
        this.body = in.readString();
    }

    public static final Parcelable.Creator<ObjCommentView> CREATOR = new Parcelable.Creator<ObjCommentView>() {
        @Override
        public ObjCommentView createFromParcel(Parcel source) {
            return new ObjCommentView(source);
        }

        @Override
        public ObjCommentView[] newArray(int size) {
            return new ObjCommentView[size];
        }
    };
}
