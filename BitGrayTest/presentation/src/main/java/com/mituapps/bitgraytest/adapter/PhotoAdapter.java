package com.mituapps.bitgraytest.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mituapps.bitgraytest.R;
import com.mituapps.bitgraytest.models.ObjPhotoView;
import com.mituapps.bitgraytest.utils.media.ImageLoaderHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andres Rubiano Del Chiaro on 19/09/2016.
 */

public class PhotoAdapter extends RecyclerView.Adapter<PhotoAdapter.RecyclerViewHolder> {

    private ArrayList<ObjPhotoView> mItems = new ArrayList<>();
    private ImageLoaderHelper imageLoaderHelper;
    private Context context;

    public PhotoAdapter(Context context) {
        this.context = context;
        this.imageLoaderHelper= new ImageLoaderHelper(ImageLoaderHelper.GLIDE);
    }

    public PhotoAdapter(Context context, ArrayList<ObjPhotoView> mItems) {
        this.mItems = mItems;
        this.context = context;
        this.imageLoaderHelper= new ImageLoaderHelper(ImageLoaderHelper.GLIDE);
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_photo, parent, false);
        return new RecyclerViewHolder(v);
    }

    public void addAll(List<ObjPhotoView> items) {
        int pos = getItemCount();
        mItems.addAll(items);
        notifyItemRangeInserted(pos, mItems.size());
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }


    class RecyclerViewHolder extends RecyclerView.ViewHolder {
        private TextView mLabelTitle;
        private TextView mLabelSubTitle;
        private ImageView mImageView;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            mLabelTitle = (TextView) itemView.findViewById(R.id.label_photo_title);
            mLabelSubTitle = (TextView) itemView.findViewById(R.id.label_photo_url);
            mImageView = (ImageView) itemView.findViewById(R.id.img_sampleimage);
        }

        public void bind(int position) {
            imageLoaderHelper.getLoader().load(
                                        mItems.get(position).getUrl(),
                                        mImageView);
            mLabelTitle.setText(String.valueOf(mItems.get(position).getTitle()));
            mLabelSubTitle.setText(mItems.get(position).getUrl());
        }
    }

}
