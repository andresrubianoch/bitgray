package com.mituapps.bitgraytest.utils.animation;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andres Rubiano Del Chiaro on 15/02/16.
 */
public class ModelItem {
    private String author;
    private int imgId;

    public ModelItem(String author, int imgId) {
        this.author = author;
        this.imgId = imgId;
    }

    public int getImgId() {
        return imgId;
    }

    public String getAuthor() {
        return author;
    }

    public static List<ModelItem> getFakeItems() {
        ArrayList<ModelItem> itemsList = new ArrayList<>();
//        itemsList.add(new ModelItem("Darren J Bennet",R.mipmap.ic_launcher));
//        itemsList.add(new ModelItem("Besim  Mazhiqi", R.mipmap.ic_launcher));
//        itemsList.add(new ModelItem("Mark Bridger", R.mipmap.ic_launcher));
//        itemsList.add(new ModelItem("William Mevissem", R.mipmap.ic_launcher));
//        itemsList.add(new ModelItem("Darren J Bennet", R.mipmap.ic_launcher));
        return itemsList;
    }
}