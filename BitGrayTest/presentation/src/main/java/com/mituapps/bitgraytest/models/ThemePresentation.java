package com.mituapps.bitgraytest.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Andres Rubiano Del Chiaro on 24/09/2016.
 */

public class ThemePresentation implements Parcelable {
    private String urlImage;
    private String title;
    private String subTitle;

    public ThemePresentation() {
    }

    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.urlImage);
        dest.writeString(this.title);
        dest.writeString(this.subTitle);
    }

    protected ThemePresentation(Parcel in) {
        this.urlImage = in.readString();
        this.title = in.readString();
        this.subTitle = in.readString();
    }

    public static final Creator<ThemePresentation> CREATOR = new Creator<ThemePresentation>() {
        @Override
        public ThemePresentation createFromParcel(Parcel source) {
            return new ThemePresentation(source);
        }

        @Override
        public ThemePresentation[] newArray(int size) {
            return new ThemePresentation[size];
        }
    };
}
