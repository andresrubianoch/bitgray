package com.mituapps.bitgraytest.ui.fragments;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mituapps.bitgraytest.R;
import com.mituapps.bitgraytest.adapter.CommentAdapter;
import com.mituapps.bitgraytest.models.ObjCommentView;
import com.mituapps.bitgraytest.ui.activities.CommentsActivity;
import com.mituapps.bitgraytest.ui.recycler.RecyclerClickListener;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A placeholder fragment containing a simple view.
 */
public class CommentsFragment extends BaseFragment {

    @Bind(R.id.post_text_title)
    TextView postTextTitle;
    @Bind(R.id.toolbar_comment)
    Toolbar toolbarFragment;
    @Bind(R.id.recyclerComment)
    RecyclerView recyclerComment;

    private ArrayList<ObjCommentView> listPost;
    private CommentAdapter mAdapter;

    public CommentsFragment() {
    }

    public static CommentsFragment newInstance(Bundle arguments) {
        CommentsFragment f = new CommentsFragment();
        if (arguments != null) {
            f.setArguments(arguments);
        }
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_comments, container, false);
        ButterKnife.bind(this, view);
        initList();
        setRetainInstance(true);

        initToolbar();
        initRecycler();
        initAdapter();
        return view;
    }

    private void initToolbar() {
        setUpToolbar(toolbarFragment);
    }

    private void initRecycler() {
        setUpRecycler(postTextTitle, recyclerComment);
    }

    private void initAdapter() {
        mAdapter = new CommentAdapter(getActivity());
        recyclerComment.setAdapter(mAdapter);
        ObjCommentView al = new ObjCommentView("id labore ex et quam laborum", "Eliseo@gardner.biz", "laudantium enim quasi est quidem magnam voluptate ipsam eos\\ntempora quo necessitatibus\\ndolor quam autem quasi\\nreiciendis et nam sapiente accusantium");
        final ArrayList<ObjCommentView> list = new ArrayList<>();
        list.add(al);
        al = new ObjCommentView("quo vero reiciendis velit similique earum", "Jayne_Kuhic@sydney.com", "est natus enim nihil est dolore omnis voluptatem numquam\\net omnis occaecati quod ullam at\\nvoluptatem error expedita pariatur\\nnihil sint nostrum voluptatem reiciendis et");
        list.add(al);
        al = new ObjCommentView("odio adipisci rerum aut animi", "Nikita@garfield.biz", "quia molestiae reprehenderit quasi aspernatur\\naut expedita occaecati aliquam eveniet laudantium\\nomnis quibusdam delectus saepe quia accusamus maiores nam est\\ncum et ducimus et vero voluptates excepturi deleniti ratione");
        list.add(al);
        al = new ObjCommentView("alias odio sit", "Lew@alysha.tv", "non et atque\\noccaecati deserunt quas accusantium unde odit nobis qui voluptatem\\nquia voluptas consequuntur itaque dolor\\net qui rerum deleniti ut occaecati");
        list.add(al);
        al = new ObjCommentView("vero eaque aliquid doloribus et culpa", "Hayden@althea.biz", "harum non quasi et ratione\\ntempore iure ex voluptates in ratione\\nharum architecto fugit inventore cupiditate\\nvoluptates magni quo et");
        list.add(al);

        setUpToolbarAnimation(toolbarFragment, new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                mAdapter.addAll(list);
            }
        });
    }

    private void initList() {
        listPost = new ArrayList<>();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
