package com.mituapps.bitgraytest.ui.fragments;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mituapps.bitgraytest.R;
import com.mituapps.bitgraytest.adapter.PostAdapter;
import com.mituapps.bitgraytest.models.ObjPostView;
import com.mituapps.bitgraytest.ui.activities.CommentsActivity;
import com.mituapps.bitgraytest.ui.recycler.RecyclerClickListener;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

public class PostFragment extends BaseFragment {

    @Bind(R.id.post_text_title)
    TextView postTextTitle;
    @Bind(R.id.toolbar_post)
    Toolbar toolbarFragment;
    @Bind(R.id.recyclerPost)
    RecyclerView recyclerPost;

    private ArrayList<ObjPostView> listPost;
    private PostAdapter mAdapter;

    public PostFragment() {
    }

    public static PostFragment newInstance(Bundle arguments) {
        PostFragment f = new PostFragment();
        if (arguments != null) {
            f.setArguments(arguments);
        }
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_post, container, false);
        ButterKnife.bind(this, view);
        initList();
        setRetainInstance(true);

        initToolbar();
        initRecycler();
        initAdapter();
        return view;
    }

    private void initToolbar() {
        setUpToolbar(toolbarFragment);
    }

    private void initRecycler() {
        setUpRecycler(postTextTitle, recyclerPost,
                new RecyclerClickListener() {
                    @Override
                    public void onClick(View view, int position) {
                        Intent intent = new Intent(getActivity(), CommentsActivity.class);
                        getActivity().startActivity(intent);
                    }

                    @Override
                    public void onLongClick(View view, int position) {

                    }
                });
    }

    private void initAdapter() {
        mAdapter = new PostAdapter(getActivity());
        recyclerPost.setAdapter(mAdapter);
        ObjPostView al = new ObjPostView(1, 1, "reprehenderit est deserunt velit ipsam", "http://placehold.it/600/92c952");
        final ArrayList<ObjPostView> list = new ArrayList<>();
        list.add(al);
        al = new ObjPostView(1, 1, "reprehenderit est deserunt velit ipsam", "http://placehold.it/600/771796");
        list.add(al);
        al = new ObjPostView(1, 1, "officia porro iure quia iusto qui ipsa ut modi", "http://placehold.it/600/24f355");
        list.add(al);
        al = new ObjPostView(1, 1, "culpa odio esse rerum omnis laboriosam voluptate repudiandae", "http://placehold.it/600/d32776");
        list.add(al);
        al = new ObjPostView(1, 1, "natus nisi omnis corporis facere molestiae rerum in", "http://placehold.it/600/f66b97");
        list.add(al);
        al = new ObjPostView(1, 1, "accusamus ea aliquid et amet sequi nemo", "http://placehold.it/600/56a8c2");
        list.add(al);
        al = new ObjPostView(1, 1, "officia delectus consequatur vero aut veniam explicabo molestias", "http://placehold.it/600/b0f7cc");
        list.add(al);

        setUpToolbarAnimation(toolbarFragment, new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                mAdapter.addAll(list);
            }
        });
    }

    private void initList() {
        listPost = new ArrayList<>();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
