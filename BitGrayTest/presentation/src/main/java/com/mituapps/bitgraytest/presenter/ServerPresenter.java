package com.mituapps.bitgraytest.presenter;

import com.interactor.ServerCallback;
import com.interactor.SignInInteractor;
import com.mituapps.bitgraytest.R;
import com.mituapps.bitgraytest.view.MainView;
import com.mituapps.data.datasource.DataStoreFactory;
import com.mituapps.data.mapper.DataMapper;
import com.mituapps.data.repository.SignInRepositoryImpl;
import com.mituapps.data.repository.DataRepositoryImpl;
import com.model.User;
import com.repository.SignInRepository;

import java.util.Random;

/**
 * Created by Andres Rubiano Del Chiaro on 16/04/16.
 */
public class ServerPresenter implements Presenter<MainView>, ServerCallback
{
    private static final String TAG = "SignInPresenter";
    private static final int ERR_NO_INTERNET = 1;
    private static final int ERR_CREDENTIALS = 2;
    private MainView mainView;
    private SignInInteractor themeInteractor;

    public void onSignIn(boolean statusConnection) {
        this.mainView.enableInputs(false);
        this.mainView.showLoading();
        if (statusConnection) {
            if (!this.mainView.validateEmail()) {
                this.mainView.enableInputs(true);
                this.mainView.hideLoading();
                return;
            }

            if (!this.mainView.validatePassword()) {
                this.mainView.hideLoading();
                this.mainView.enableInputs(true);
                return;
            }

            if (!this.mainView.validateCredentials()) {
                onThemeError(ERR_CREDENTIALS);
                return;
            }

            this.themeInteractor.onSignIn(this, getUserId());
        } else {
            onThemeError(ERR_NO_INTERNET);
        }
    }

    @Override
    public void addView(MainView view) {
        this.mainView = view;
//        SignInRepository themeRepository = new SignInRepositoryImpl(new DataStoreFactory(this.mainView.getContext()),new DataMapper());
        SignInRepository themeRepository = new DataRepositoryImpl(new DataStoreFactory(this.mainView.getContext()),new DataMapper());
        themeInteractor = new SignInInteractor(themeRepository);
    }

    @Override
    public void removeView(MainView view) {
        this.mainView = null;
    }

    @Override
    public void onThemeSuccess(User user) {
        if (null != this.mainView){
            this.mainView.hideLoading();
            this.mainView.addUserSession(user);
            this.mainView.goToMenuActivity();
        }

//            this.mainView.onSuccessDownloaded(themes);
    }

    public void onThemeError(int message) {
        if (null != this.mainView){
            if (message == ERR_NO_INTERNET) {
                this.mainView.noInternetConnection();
            } else if (message == ERR_CREDENTIALS) {
                this.mainView.showErrorMessage(this.mainView.getContext().getString(R.string.err_msg_credential));
            }
            this.mainView.hideLoading();
            this.mainView.enableInputs(true);
        }
    }

    @Override
    public void onThemeError(String message) {
    }

    public int getUserId() {
        Random random = new Random();
        return (Math.abs(random.nextInt() % 10) + 1);
    }
}
