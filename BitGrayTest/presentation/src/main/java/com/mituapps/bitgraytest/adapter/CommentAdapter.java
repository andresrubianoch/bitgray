package com.mituapps.bitgraytest.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mituapps.bitgraytest.R;
import com.mituapps.bitgraytest.models.ObjCommentView;
import com.mituapps.bitgraytest.utils.media.ImageLoaderHelper;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Andres Rubiano Del Chiaro on 19/09/2016.
 */

public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.RecyclerViewHolder> {


    private ArrayList<ObjCommentView> mItems = new ArrayList<>();
    private ImageLoaderHelper imageLoaderHelper;
    private Context context;

    public CommentAdapter(Context context) {
        this.context = context;
        this.imageLoaderHelper = new ImageLoaderHelper(ImageLoaderHelper.GLIDE);
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_comment_row, parent, false);
        return new RecyclerViewHolder(v);
    }

    public void addAll(List<ObjCommentView> items) {
        int pos = getItemCount();
        mItems.addAll(items);
        notifyItemRangeInserted(pos, mItems.size());
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }


    class RecyclerViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.labelCommentName)
        TextView labelCommentName;
        @Bind(R.id.labelCommentEmail)
        TextView labelCommentEmail;
        @Bind(R.id.labelCommentBody)
        TextView labelCommentBody;

        public RecyclerViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(int position) {
            labelCommentName.setText(mItems.get(position).getName());
            labelCommentEmail.setText(mItems.get(position).getEmail());
            labelCommentBody.setText(mItems.get(position).getBody());
        }
    }

}
