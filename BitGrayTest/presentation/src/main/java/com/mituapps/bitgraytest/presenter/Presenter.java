package com.mituapps.bitgraytest.presenter;

/**
 * Created by Andres Rubiano Del Chiaro on 16/04/16.
 */
public interface Presenter<T> {

    void addView(T view);
    void removeView(T view);
}
