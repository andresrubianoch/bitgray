package com.mituapps.data.model;

import java.util.List;

/**
 * Created by Andres Rubiano Del Chiaro on 16/04/16.
 */
public class SignInResponse extends BaseResponse {

    private int offset;
    private List<SignInEntity> data;
    private Object nextPage;
    private int totalObjects;

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public List<SignInEntity> getData() {
        return data;
    }

    public void setData(List<SignInEntity> data) {
        this.data = data;
    }

    public Object getNextPage() {
        return nextPage;
    }

    public void setNextPage(Object nextPage) {
        this.nextPage = nextPage;
    }

    public int getTotalObjects() {
        return totalObjects;
    }

    public void setTotalObjects(int totalObjects) {
        this.totalObjects = totalObjects;
    }
}
