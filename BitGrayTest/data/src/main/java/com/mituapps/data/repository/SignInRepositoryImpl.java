package com.mituapps.data.repository;

import com.interactor.ServerCallback;
import com.mituapps.data.datasource.MethodsDataStore;
import com.mituapps.data.datasource.DataStoreFactory;
import com.mituapps.data.mapper.DataMapper;
import com.mituapps.data.model.SignInEntity;
import com.model.User;
import com.repository.RepositoryCallback;
import com.repository.SignInRepository;

/**
 * Created by Andres Rubiano Del Chiaro on 22/04/16.
 */
public class SignInRepositoryImpl implements SignInRepository {

    private static final String TAG = "UserDataRepository";
    private final DataStoreFactory dataStoreFactory;
    private final DataMapper dataMapper;

    public SignInRepositoryImpl(DataStoreFactory dataStoreFactory, DataMapper dataMapper) {
        this.dataStoreFactory = dataStoreFactory;
        this.dataMapper = dataMapper;
    }

    @Override
    public void onSignIn(final ServerCallback signInCallback, int idUser) {
        final MethodsDataStore singInDataStore= this.dataStoreFactory.create(DataStoreFactory.CLOUD);
        singInDataStore.onSignIn(idUser, new RepositoryCallback() {
            @Override
            public void onError(Object object) {
                String message = "";
                if(object!=null){
                    message= object.toString();
                }
                signInCallback.onThemeError(message);
            }

            @Override
            public void onSuccess(Object object) {
                SignInEntity userEntity = ((SignInEntity) (object));
                User user = dataMapper.transformResponse(userEntity);

                signInCallback.onThemeSuccess(user);
            }
        });
    }

    public void onGetAlbums(final ServerCallback signInCallback, int idUser) {
        final MethodsDataStore singInDataStore= this.dataStoreFactory.create(DataStoreFactory.CLOUD);
        singInDataStore.getAlbums(idUser, new RepositoryCallback() {
            @Override
            public void onError(Object object) {
                String message = "";
                if(object!=null){
                    message= object.toString();
                }
                signInCallback.onThemeError(message);
            }

            @Override
            public void onSuccess(Object object) {
                SignInEntity userEntity = ((SignInEntity) (object));
                User user = dataMapper.transformResponse(userEntity);

                signInCallback.onThemeSuccess(user);
            }
        });
    }
}
