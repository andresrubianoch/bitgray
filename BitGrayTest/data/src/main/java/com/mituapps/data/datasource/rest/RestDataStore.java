package com.mituapps.data.datasource.rest;

import android.util.Log;

import com.mituapps.data.datasource.MethodsDataStore;
import com.mituapps.data.model.AlbumsEntity;
import com.mituapps.data.model.SignInEntity;
import com.repository.RepositoryCallback;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Andres Rubiano Del Chiaro on 16/04/16.
 */
public class RestDataStore implements MethodsDataStore {

    private static final String TAG = "RestPlaceDataS";
    private ApiClient.ServicesApiInterface servicesApiInterface;

    public RestDataStore() {
        servicesApiInterface= ApiClient.getMyApiClient();
    }

    @Override
    public void onSignIn(int idUser, final RepositoryCallback repositoryCallback) {
        servicesApiInterface.users(idUser, new Callback<SignInEntity>() {
            @Override
            public void success(SignInEntity placeResponse, Response response) {
                if(placeResponse!=null) {
                    repositoryCallback.onSuccess(placeResponse);
                }else{
                    repositoryCallback.onError("");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                String message="";
                if(error!=null) {
                    message= error.getMessage();
                }
                Log.v(TAG,"error "+message);
                repositoryCallback.onError(message);
            }
        });
    }

    @Override
    public void getAlbums(int idUser, final RepositoryCallback repositoryCallback) {
        servicesApiInterface.getAlbums(idUser, new Callback<AlbumsEntity>() {
            @Override
            public void success(AlbumsEntity albumsResponse, Response response) {
                if(albumsResponse!=null) {
                    repositoryCallback.onSuccess(albumsResponse);
                }else{
                    repositoryCallback.onError("");
                }
            }

            @Override
            public void failure(RetrofitError error) {
                String message="";
                if(error!=null) {
                    message= error.getMessage();
                }
                Log.v(TAG,"error "+message);
                repositoryCallback.onError(message);
            }
        });
    }
}
