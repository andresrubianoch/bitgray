package com.mituapps.data.model;

import java.util.ArrayList;

/**
 * Created by Andres Rubiano Del Chiaro on 28/09/2016.
 */
public class AlbumResponse extends BaseResponse{

    private int offset;
    private ArrayList<AlbumsEntity> data;
    private Object nextPage;
    private int totalObjects;

    public AlbumResponse() {
    }

    public ArrayList<AlbumsEntity> getData() {
        return data;
    }

    public void setData(ArrayList<AlbumsEntity> data) {
        this.data = data;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public Object getNextPage() {
        return nextPage;
    }

    public void setNextPage(Object nextPage) {
        this.nextPage = nextPage;
    }

    public int getTotalObjects() {
        return totalObjects;
    }

    public void setTotalObjects(int totalObjects) {
        this.totalObjects = totalObjects;
    }
}
