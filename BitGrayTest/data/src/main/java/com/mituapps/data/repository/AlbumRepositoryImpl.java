package com.mituapps.data.repository;

import com.interactor.AlbumCallback;
import com.mituapps.data.datasource.DataStoreFactory;
import com.mituapps.data.datasource.MethodsDataStore;
import com.mituapps.data.mapper.DataMapper;
import com.mituapps.data.model.AlbumsEntity;
import com.model.Album;
import com.repository.AlbumRepository;
import com.repository.RepositoryCallback;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andres Rubiano Del Chiaro  on 27/09/2016.
 */

public class AlbumRepositoryImpl implements AlbumRepository{

    private final DataStoreFactory dataStoreFactory;
    private final DataMapper dataMapper;

    public AlbumRepositoryImpl(DataStoreFactory dataStoreFactory, DataMapper dataMapper) {
        this.dataStoreFactory = dataStoreFactory;
        this.dataMapper = dataMapper;
    }

    @Override
    public void getAlbums(final AlbumCallback albumCallback, int idUser) {
        final MethodsDataStore albumDataStore = this.dataStoreFactory.create(DataStoreFactory.CLOUD);
        albumDataStore.getAlbums(idUser, new RepositoryCallback() {
            @Override
            public void onError(Object object) {
                String message = "";
                if(object!=null){
                    message= object.toString();
                }
                albumCallback.onAlbumError(message);
                albumCallback.onAlbumSuccess(getAlbums());
            }

            @Override
            public void onSuccess(Object object) {
                AlbumsEntity albumEntity = ((AlbumsEntity) (object));
                Album album = dataMapper.transformResponse(albumEntity);

                albumCallback.onAlbumSuccess(getAlbums());
            }
        });
    }

    @Override
    public void getPhotos(AlbumCallback signInCallback, int idUser) {

    }

    private List<Album> getAlbums(){
        List<Album> list = new ArrayList<>();
        list.add(new Album(5, 41, "ea voluptates maiores eos accusantium officiis tempore mollitia consequatur"));
        list.add(new Album(5, 42, "tenetur explicabo ea"));
        list.add(new Album(5, 43, "aperiam doloremque nihil"));
        list.add(new Album(5, 44, "sapiente cum numquam officia consequatur vel natus quos suscipit"));
        list.add(new Album(5, 45, "tenetur quos ea unde est enim corrupti qui"));
        list.add(new Album(5, 46, "molestiae voluptate non"));
        list.add(new Album(5, 47, "temporibus molestiae aut"));
        list.add(new Album(5, 48, "modi consequatur culpa aut quam soluta alias perspiciatis laudantium"));
        list.add(new Album(5, 49, "ut aut vero repudiandae voluptas ullam voluptas at consequatur"));
        list.add(new Album(5, 50, "sed qui sed quas sit ducimus dolor"));
        return list;
    }
}
