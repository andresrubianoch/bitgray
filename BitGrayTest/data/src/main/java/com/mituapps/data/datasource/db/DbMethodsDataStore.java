package com.mituapps.data.datasource.db;


import com.mituapps.data.datasource.MethodsDataStore;
import com.repository.RepositoryCallback;

/**
 * Created by Andres Rubiano Del Chiaro on 16/04/16.
 */
public class DbMethodsDataStore implements MethodsDataStore {

    public DbMethodsDataStore() {

    }

    @Override
    public void onSignIn(int idUser, RepositoryCallback repositoryCallback) {

    }

    @Override
    public void getAlbums(int idUser, RepositoryCallback repositoryCallback) {

    }
}
