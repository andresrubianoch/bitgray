package com.mituapps.data.model;

/**
 * Created by Andres Rubiano Del Chiaro on 27/09/2016.
 */

public class PostEntity {

    private int userId;
    private int id;
    private String title;
    private String body;

    public PostEntity() {
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
