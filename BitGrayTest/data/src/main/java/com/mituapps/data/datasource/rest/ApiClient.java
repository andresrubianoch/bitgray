package com.mituapps.data.datasource.rest;

import com.mituapps.data.model.AlbumResponse;
import com.mituapps.data.model.AlbumsEntity;
import com.mituapps.data.model.CommentsEntity;
import com.mituapps.data.model.PhotosEntity;
import com.mituapps.data.model.PostEntity;
import com.mituapps.data.model.SignInEntity;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.ResponseBody;

import java.util.concurrent.TimeUnit;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.http.GET;
import retrofit.http.Headers;
import retrofit.http.Path;

/**
 * Created by Andres Rubiano Del Chiaro on 16/04/16.
 */
public class ApiClient {

    private static final String TAG = "ApiClient";
    private static ServicesApiInterface servicesApiInterface;
    private static final String URL                     = "http://jsonplaceholder.typicode.com/";
    private static final String USER_ID                 = "/users/{idUser}";
    private static final String USER_ALBUMS             = USER_ID + "/albums";
    private static final String USER_ALBUMS_PHOTOS      = "/albums/{idUser}/photos";
    private static final String USER_POST               = USER_ID + "/posts";
    private static final String USER_POST_COMMENT       = "/posts/{idUser}/comments";

    public static ServicesApiInterface getMyApiClient() {

        if (servicesApiInterface == null) {

            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setEndpoint(URL)
                    .setClient(new OkClient(getClient()))
                    .setLogLevel(RestAdapter.LogLevel.FULL)
                    .build();

            servicesApiInterface = restAdapter.create(ServicesApiInterface.class);
        }
        return servicesApiInterface;
    }

    public interface ServicesApiInterface {

        @Headers({
                "Content-Type: application/json",
                "application-type: REST"
        })

        @GET(USER_ID)
        void users(@Path("idUser") int idUser, Callback<SignInEntity> callback);

        @GET(USER_ALBUMS)
        void getAlbums(@Path("idUser") int idUser, Callback<AlbumsEntity> callback);

        @GET(USER_ALBUMS_PHOTOS)
        void getPhotosFromAlbum(@Path("idUser") int idUser, Callback<PhotosEntity> callback);

        @GET(USER_POST)
        void getPost(@Path("idUser") int idUser, Callback<PostEntity> callback);

        @GET(USER_POST_COMMENT)
        void getCommentFromPost(@Path("idUser") int idUser, Callback<CommentsEntity> callback);
    }

    private static OkHttpClient getClient() {
        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(2, TimeUnit.MINUTES);
        client.setReadTimeout(2, TimeUnit.MINUTES);
        return client;
    }
}
