package com.mituapps.data.datasource.preferences;

import android.content.Context;

import com.mituapps.data.datasource.MethodsDataStore;
import com.repository.RepositoryCallback;

/**
 * Created by Andres Rubiano Del Chiaro on 22/04/16.
 */
public class PreferencesMethodsDataStore implements MethodsDataStore {

    private final Context context;
    public PreferencesMethodsDataStore(Context context) {
        this.context= context;
    }

    @Override
    public void onSignIn(int idUser, RepositoryCallback repositoryCallback) {

    }

    @Override
    public void getAlbums(int idUser, RepositoryCallback repositoryCallback) {
        
    }
}
