package com.mituapps.data.datasource;

import android.content.Context;

import com.mituapps.data.datasource.db.DbMethodsDataStore;
import com.mituapps.data.datasource.preferences.PreferencesMethodsDataStore;
import com.mituapps.data.datasource.rest.RestDataStore;


/**
 * Created by Andres Rubiano Del Chiaro on 24/09/16.
 */
public class DataStoreFactory {

    public static final  int DB=1;
    public static final  int CLOUD=2;
    public static final  int PREFERENCES=3;

    private final Context context;

    public DataStoreFactory(Context context) {

        if (context == null) {
            throw new IllegalArgumentException("Constructor parameters cannot be null!!!");
        }
        this.context = context;
    }

    public MethodsDataStore create(int dataSource)
    {
        MethodsDataStore methodsDataStore =null;

        switch (dataSource)
        {
            case CLOUD:
                methodsDataStore = createCloudDataStore();
                break;
            case DB:
                methodsDataStore = new DbMethodsDataStore();
                break;
            case PREFERENCES:
                methodsDataStore = new PreferencesMethodsDataStore(context);
                break;
        }
        return methodsDataStore;
    }


    public MethodsDataStore createCloudDataStore() {

        return new RestDataStore();
    }
}
