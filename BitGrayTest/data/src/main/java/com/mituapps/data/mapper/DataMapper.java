package com.mituapps.data.mapper;


import com.mituapps.data.model.AlbumsEntity;
import com.mituapps.data.model.SignInEntity;
import com.model.Album;
import com.model.User;

/**
 * Created by Andres Rubiano Del Chiaro on 16/04/16.
 */
public class DataMapper {

    public User transformResponse(SignInEntity user) {
        User newUser = new User();
        if (user != null){
            newUser.setId(user.getId());
            newUser.setName(user.getName());
            newUser.setUsername(user.getUsername());
            newUser.setEmail(user.getEmail());
            newUser.setNumber(user.getPhone());
            newUser.setCompany(user.getCompany().getName());
        }
        return newUser;
    }

    public Album transformResponse(AlbumsEntity albumsEntity) {
        Album album = new Album();
        if (albumsEntity != null){
            album.setUserId(albumsEntity.getUserId());
            album.setId(albumsEntity.getId());
            album.setTitle(albumsEntity.getTitle());
        }
        return album;
    }

}
